crispy-doom (5.12.0-1) unstable; urgency=medium

  * New upstream version 5.12.0
  * Try to shut up lintian.
  * Bump Standards-Version to 4.6.1.
  * Update my Debian packaging copyright.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 01 Sep 2022 22:26:35 +0200

crispy-doom (5.11.1-1) unstable; urgency=medium

  * New upstream version 5.11.1
  * Improve crispy-hexen integration.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 17 Feb 2022 09:23:22 +0100

crispy-doom (5.10.3-1) unstable; urgency=medium

  * Fix debian/watch file.
  * New upstream version 5.10.3
  * Bump Standards-Version to 4.5.1.
  * Remove leftover Hexen and Strife Makefiles in the clean rule.
  * Add lintian source overrides for uninstalled examples of the
    utility libraries.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 19 Aug 2021 21:43:11 +0200

crispy-doom (5.10.0-1) unstable; urgency=medium

  * Recommend freedoom instead of freedm.
  * Bump Breaks against the freedoom package to a version which is
    known to be Vanilla (and thus Crispy) Doom compatible.
  * New upstream version 5.10.0
  * Removed all patches applied upstream.

 -- Fabian Greffrath <fabian@debian.org>  Wed, 27 Jan 2021 22:39:21 +0100

crispy-doom (5.9.2-1) unstable; urgency=medium

  * New upstream version 5.9.2
  * Add some post-release patches from upstream.
  * Symlink crispy-doom-setup.6 to crispy-setup.6.
  * Remove --as-needed linker flag.
  * Bump debhelper-compat to 13.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 02 Oct 2020 09:04:57 +0200

crispy-doom (5.9.0-1) unstable; urgency=medium

  * New upstream version 5.9.0
    + Fixes CVE-2020-14983 (Closes: #964564).
  * Remove all patches applied upstream.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 21 Aug 2020 22:29:13 +0200

crispy-doom (5.8.0-2) unstable; urgency=medium

  * Fix file conflict with the chocolate-doom package,
    thanks Sven Joachim, closes: #958075.
  * Improve crispy-heretic integration.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 23 Apr 2020 21:42:49 +0200

crispy-doom (5.8.0-1) unstable; urgency=medium

  * New upstream version 5.8.0
  * Apply some post-release patches from the upstream GIT repository.
    + Un-name the color translation enum{},
      fixes FTBFS with gcc-10 (Closes: #957110).

 -- Fabian Greffrath <fabian@debian.org>  Fri, 17 Apr 2020 21:35:28 +0200

crispy-doom (5.6.4-1) unstable; urgency=medium

  * New upstream version 5.6.4
  * Bump Standards-Version to 4.5.0.
  * Include pkg-info.mk in debian/rules.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 15 Feb 2020 23:25:45 +0100

crispy-doom (5.6.3-2) unstable; urgency=medium

  [ Jonathan Dowland ]
  * Remove myself from Uploaders.

  [ Moritz Muehlenhoff ]
  * Also switch Build-Depends to python3

 -- Moritz Muehlenhoff <jmm@debian.org>  Mon, 02 Dec 2019 22:59:01 +0100

crispy-doom (5.6.3-1) unstable; urgency=medium

  * New upstream version 5.6.3
  * Account for using reverse-DNS for the project identifier in the
    desktop files.
  * Remove generated metainfo files in the package clean rule.
  * Rules-Requires-Root: no.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 11 Oct 2019 20:10:12 +0200

crispy-doom (5.6.1-1) unstable; urgency=medium

  * New upstream version 5.6.1
  * Port over Stephen Kitt's patch from the Chocolate Doom package
    to build with Python 3 (Closes: #936342).

 -- Fabian Greffrath <fabian@debian.org>  Tue, 03 Sep 2019 12:40:07 +0200

crispy-doom (5.6-1) unstable; urgency=medium

  * New upstream version 5.6
  * Do not install the server, but do not patch
    the build system either.
  * Point Homepage fields to the GitHub project page.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 05 Aug 2019 08:57:40 +0200

crispy-doom (5.5.2+git114-1) unstable; urgency=medium

  * New upstream development snapshot from GIT
    commit b4d87dde.
  * Bump Standards-Version to 4.4.0.
  * Bump debhelper-compat to 12.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 25 Jul 2019 13:32:59 +0200

crispy-doom (5.5.1-1) unstable; urgency=medium

  * New upstream version 5.5.1
  * Remove all patches backported from upstream.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 09 Mar 2019 20:09:55 +0100

crispy-doom (5.5-1) unstable; urgency=medium

  * New upstream version 5.5
  * Remove all patches backported from upstream,
    apply two post-release patches.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 01 Mar 2019 19:44:50 +0100

crispy-doom (5.4-3) unstable; urgency=medium

  * Apply some patches from the upstream GIT repository that
    fix a crash when changing the renderer in-game.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 22 Dec 2018 15:26:35 +0100

crispy-doom (5.4-2) unstable; urgency=medium

  * Pass BUILD_DATE as a string.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 18 Dec 2018 23:23:15 +0100

crispy-doom (5.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field.

  [ Fabian Greffrath ]
  * New upstream version 5.4.
  * Remove debian/missing-sources, README.Crispy.htm has been
    removed upstream.
  * Remove lintian-overrides, the INSTALL.doom document isn't
    even installed anymore.
  * Fix typo in package description, Closes: #916600.
  * Remove non-existent patch from series file.
  * Bump debian compat to 11.
  * Replace README.Crispy.htm with README.md in debian/rules.
  * Remove CC-BY-SA-4.0 from debian/copyright.
  * Fix some removed or moved files in debian/copyright.
  * Enable all hardening flags.
  * Make build reproducible.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 18 Dec 2018 22:32:57 +0100

crispy-doom (5.2-1) unstable; urgency=medium

  * Initial packaging based on chocolate-doom.
    Closes: #880972.
  * We do not build or install the crispy-server program: it is identical
    to, and designed to be compatible with, the pre-existing
    chocolate-server.
  * Lintian has two false positives (privacy-breach-generic), see
    #902919 for details.

 -- Jonathan Dowland <jmtd@debian.org>  Thu, 05 Jul 2018 14:37:21 +0100
